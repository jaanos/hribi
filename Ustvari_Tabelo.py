import sqlite3
import os
import sys
import csv
import re 
import codecs

import psycopg2, psycopg2.extensions, psycopg2.extras
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE) # se znebimo problemov s sumniki


## Pomožne funkcije######################################################################################
def poberi_ime(niz):
    """ To funkcijo bomo uporabili, da odstranimo podatek o višini hribov iz okolice posameznega hriba. Služi lažjemu pregledovanju podatkov."""
    ## Definiramo posabna imena hribov, ki jih drugače ne znam obravnavat
    posebnosti = ["Soteska Pekel 3. slap","Soteska Pekel 5. slap"]
    for i in posebnosti:
        m = re.match(i,niz)
        try:
            vrni = m.group()
            return vrni
        except AttributeError:
            pass
        
    ## Splošen primer (nečudno ime)
    p = re.compile('\D+')
    m = p.match(niz)
    return m.group()[:-1]


def stevilka_hribovja(hribovje):
    slovar = {"julijske":1,"kamnisko_savinjske":2,"karavanke":3,"notranjsko":4,"pohorje":5,"polhograjsko":6,
              "posavsko":7,"skofjelosko":8}
    return slovar[hribovje]


def preberi_okolico(okolica):
    """okolica je oblike str """
    ok = okolica.split(",")
    for i in range(len(ok)):
        ok[i] = ok[i][0:len(ok[i])-1]
    if ok == ['']:
        return []
    return ok
        

def Vrni_id_hriba(hrib):
    baza = psycopg2.connect(database='seminarska_benjaminmm', host='baza.fmf.uni-lj.si', user='benjaminmm', password='Challenge1807040')
    cursor = baza.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute('''SELECT id FROM hribi_tabela WHERE ime=%s ''',(hrib,))
    return cursor.fetchone()[0]

def pretvori_uro_v_int(niz):
    p = re.compile('[0-9]+')
    m = p.findall(niz)
    ura = 0
    if len(m) == 1:
        ura = int(m[0])*60
    else:
        ura = int(m[0])*60
        ura += int(m[1])
    return ura



## konec pomožnih funkcij ####################################################################################








def ustvari_bazo(ime_csv):
    """ime_csv in ime ustavi brez koncnic! S to funkcijo naredimo bazo in tabelo hribi.
naslednje tabele naredimo z novimi funkcijami."""
    ime_baze = "hribi_baza"
    
##ustvarimo bazo če še ne obstaja.
    BAZA = ime_baze+".db"
    baza = psycopg2.connect(database='seminarska_benjaminmm', host='baza.fmf.uni-lj.si', user='benjaminmm', password='Challenge1807040')
    cursor = baza.cursor(cursor_factory=psycopg2.extras.DictCursor)

    ## Ustvarimo tabelo hribovji
    slovar = {"julijske":1,"kamnisko_savinjske":2,"karavanke":3,"notranjsko":4,"pohorje":5,"polhograjsko":6,
              "posavsko":7,"skofjelosko":8}

    ## Naredimo da se to izvede samo enkrat. (ne bo delalp šrav če ne gremo po vrsti, najprej z julijskimi)
    if ime_csv == "julijske":
        cursor.execute('''CREATE TABLE IF NOT EXISTS hribovja
                       (id INTEGER PRIMARY KEY,                
                       ime_hribovja TEXT
                       )''')
        
        for i in slovar:
            cursor.execute("""INSERT INTO hribovja VALUES (%s,%s) """,(slovar[i],i))
        baza.commit()

    cursor.execute('''CREATE TABLE IF NOT EXISTS hribi_tabela  
                   (id SERIAL PRIMARY KEY,
                   ime TEXT,
                   hribovje INTEGER,
                   višina INTEGER,
                   trenutna_temperatura INTEGER,
                   priljubljenost TEXT,
                   FOREIGN KEY(hribovje) REFERENCES hribovja(id)
                   )''')

## Najprej bom prebrau csv datoteko in lepo označil podatke, da se ve kaj je kaj.
## Mogoče (verjetno) je to ena zmeda.
    
    ime_csv_koncnica = ime_csv + ".csv"
    with open(ime_csv_koncnica,encoding="utf-8") as csvfile:
        
        ## preberemo podatke in jih ustavimo v bazo
        reader = csv.reader(csvfile)
        for row in reader:
            # zmasakriramo prvi element vrstice, ostali so okolica
            podatki = row[0].split("[")  ## podatki so v čudni obliki zato jih razdelim/zgleda da je "napaka" v shranjevanju v csv
            ime_hrib = poberi_ime(podatki[0])
            hrib = podatki[0][len(ime_hrib)::].split() ## iz podniza row[0] odstranimo ime hriba ostalo razbijemo po presledkih("hrib" - so podatki)
            visina_hrib = int(hrib[0])
            temp_hrib = int(hrib[1])
            priljub_hrib = hrib[2]+hrib[3]+hrib[4]
            st_poti_hrib = int(hrib[5])

            ## ustavimo podatke v bazo ## zelo dolga vrstica :)
            st_hribovja = stevilka_hribovja(ime_csv) # ko bomo sproti dajal v bazo bomo spreminjali samo ime_csv
            cursor.execute('''INSERT INTO hribi_tabela(ime,hribovje, višina, trenutna_temperatura, priljubljenost) VALUES (%s,%s,%s,%s,%s) ''',(ime_hrib,st_hribovja,visina_hrib,temp_hrib,priljub_hrib))

    baza.commit()
    baza.close()
    return





## ustvari in nato napolni tabelo. vse dela ce najprej klices to funkcije ###
def napolni_tabelo():
    seznam = ["julijske","kamnisko_savinjske","karavanke","notranjsko","pohorje","polhograjsko",
              "posavsko","skofjelosko"]
    for i in seznam:
        ustvari_bazo(i)
        


## Tukaj bomo naredili nove tabele ki bodo povezane na tabelo hribi_tabela.

def okolica_tabela():
    """Če hrib nima nebenega drugega hriba v okolici bo to v bazi označeno z NULL """
    ime_baze = "hribi_baza"
    BAZA = ime_baze+".db"
    baza = psycopg2.connect(database='seminarska_benjaminmm', host='baza.fmf.uni-lj.si', user='benjaminmm', password='Challenge1807040')
    cursor = baza.cursor(cursor_factory=psycopg2.extras.DictCursor)


    cursor.execute('''CREATE TABLE IF NOT EXISTS je_okolica
                   (hrib INTEGER REFERENCES hribi_tabela(id),
                   okoliski_hrib INTEGER REFERENCES hribi_tabela(id),
                   PRIMARY KEY (hrib,okoliski_hrib)
                   )''')

    ## prebrali bomo vrstice v okoliski_hribi in vstavljali v tabelo.
    ## če hrib nima okolice bo to zapisano z (hrib, NULL)
    okolica_dat = open("okoliski_hribi1.txt","r")
    
    for vrstica in okolica_dat:
        razbitje = vrstica.split(";")
        hrib_ime = razbitje[0]
        seznam_okolica = preberi_okolico(razbitje[1])
        print(hrib_ime)
        if len(seznam_okolica) != 0: ## zadnjemu hribu pobrišemo prazni znak na koncu imena
            seznam_okolica[-1] = seznam_okolica[-1][0:-1]
        if seznam_okolica == []:
            pass
            #id1 = Vrni_id_hriba(hrib_ime)
            #cursor.execute("INSERT INTO je_okolica VALUES (%s,%s)",(id1,0))
            
            
        else:
            id_hrb = Vrni_id_hriba(hrib_ime)
            for i in seznam_okolica:
                id_i = Vrni_id_hriba(i)
                cursor.execute("SELECT * FROM je_okolica WHERE hrib=%s AND okoliski_hrib=%s",(id_hrb,id_i))
                
                if cursor.fetchone() == None:
                
                    id1 = Vrni_id_hriba(hrib_ime)
                    prvi = id1
                    id2 = Vrni_id_hriba(i)
                    drugi = id2
                    cursor.execute("INSERT INTO je_okolica VALUES (%s,%s)",(prvi,drugi))
                
    
 
    okolica_dat.close()
    baza.commit()
    baza.close()
    return


def poti_tabela():
    """Funkcija ki bo naredila tabelo z usemi potmi. """
    ime_baze = "hribi_baza"
    BAZA = ime_baze+".db"
    baza = psycopg2.connect(database='seminarska_benjaminmm', host='baza.fmf.uni-lj.si', user='benjaminmm', password='Challenge1807040')
    cursor = baza.cursor(cursor_factory=psycopg2.extras.DictCursor)
    poti_data = open("poti.txt","r")
    
    cursor.execute('''CREATE TABLE IF NOT EXISTS poti
                   (id SERIAL PRIMARY KEY,
                   hrib INTEGER REFERENCES hribi_tabela(id),
                   ime_poti TEXT,
                   dolžina INTEGER,
                   težavnost TEXT
                   )''')
    for line in poti_data:
        seznam = line.split(",")
        for i in range(len(seznam)):
            if i == 0:
                seznam[i] = seznam[i][1:len(seznam[i])-1]
            else:
                seznam[i] = seznam[i][2:len(seznam[i])-1]
        seznam[-1] = seznam[-1][0:-1]
        if seznam != ['']: # tukaj je bil problem ker je zadnja vrstica prazna
            try:
                id_hrib = Vrni_id_hriba(seznam[0])
                cas = pretvori_uro_v_int(seznam[2])
                cursor.execute("""INSERT INTO poti(hrib,ime_poti,dolžina,težavnost) VALUES (%s,%s,%s,%s)""",(id_hrib,seznam[1],cas,seznam[3]))
            except IndexError:
                print(seznam)
                
        
    
    baza.commit()
    baza.close()
    return
    
    

























    
    




## TESTI ko bodo vse tabele narejene in dela POBRIŠI

def test5():
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute('''SELECT * FROM poti WHERE hrib =? ''',(10,))
    print(cursor.fetchall())

def test1():
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    
    cursor.execute('''SELECT ime,višina FROM hribi_tabela WHERE ime=? AND višina=?''',("Triglav",2864))
    print(cursor.fetchall())

def test2(ime):
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("""SELECT id FROM hribi_tabela WHERE ime=?""",(ime,))
    idH = cursor.fetchone()[0]
    cursor.execute('''SELECT * FROM je_okolica WHERE hrib=? ''',(idH,))
    for i in cursor.fetchall():
        (h1,h2) = i
        cursor.execute("""SELECT ime FROM hribi_tabela WHERE id=?""",(h1,))
        hh1 = cursor.fetchone()[0]
        cursor.execute("""SELECT ime FROM hribi_tabela WHERE id=?""",(h2,))
        hh2 = cursor.fetchone()[0]
        print(hh1,hh2)



def test3():
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    

    cursor.execute('''CREATE TABLE IF NOT EXISTS testna
                   (id INTEGER,
                   dodatek INTEGER,
                   PRIMARY KEY (id,dodatek)
                   )''')
    cursor.execute("INSERT INTO testna VALUES (?,?)",(1,None))
    cursor.execute("INSERT INTO testna VALUES (?,?)",(2,1))
    cursor.execute("INSERT INTO testna VALUES (?,?)",(3,1))
    cursor.execute("INSERT INTO testna VALUES (?,?)",(2,3))
    cursor.execute("INSERT INTO testna VALUES (?,?)",(2,4))
    cursor.execute("SELECT id FROM testna WHERE id=? AND dodatek=?",(2,3))
    print(cursor.fetchall())
    cursor.execute("SELECT * FROM testna WHERE id=? AND dodatek=?",(2,5))
    print(cursor.fetchone())
    if cursor.fetchall() == []:
        print("Tale ni še notr")

def test4():
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("SELECT ime FROM hribi_tabela ")
    print(cursor.fetchall())
    
def ovrzi():
    baza = sqlite3.connect("hribi_baza.db")
    cursor = baza.cursor()
    cursor.execute("DROP TABLE poti")
    baza.commit()
    baza.close()

    

##preveri
# poglej če deluje vrednost none v sqlite3 pri stolpcu integer
#
#
#
#
#
#
#
#
#
#
#
#
