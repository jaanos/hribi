from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    visine = {}
    printData = False
    printTable = False
    station = ''
    h_url = []
    def handle_starttag(self, tag, attrs):
        if tag == 'span':
            for name, value in attrs:
                if name == 'id' and value == 'gorovjaseznam':
                    self.printTable = True
        if tag == 'tr':
            self.column = 0
        if tag == 'a':
            self.column += 1
            if self.printTable == True:
                self.printData = True
            if attrs[0][0] == 'href':
                if attrs[0][1] not in self.h_url:
                    if (attrs[0][1][0] == '/') and (attrs[0][1] != '/'):
                        self.h_url.append(attrs[0][1])
                        
    def handle_endtag(self, tag):
        if tag == 'span':
            self.printTable = False
        if tag == 'a':
            self.printData = False

    def handle_data(self, data):
        if self.printData == True:
            if self.column == 1:
                self.station = data
            if self.column == 2:
                self.visine[self.station]=data     
            




class MyHTMLParser_details(HTMLParser):
    details = []
    printData = False
    printTable = False
    def handle_starttag(self, tag, attrs):
        if tag == 'table':
            for name, value in attrs:
                if name in ['align','valign','width'] and value in ['left','top','200']:
                    self.printTable = True
        if tag == 'tr':
            self.column = 0
        if tag == 'a' or tag == 'b':
            self.column += 1
            if self.printTable == True:
                self.printData = True
    def handle_endtag(self, tag):
        if tag == 'table':
            self.printTable = False
        if tag == 'a':
            self.printData = False

    def handle_data(self, data):
        if self.printData == True:
            if self.column == 1:
                self.details.append(data)
            if self.column == 2:
                self.details.append(data)




class MyHTMLParser_okolica(HTMLParser):
    okolica = []
    printData = False
    printTable = False
    station = ''
    def handle_starttag(self, tag, attrs):
        if tag == 'span':
            for name, value in attrs:
                if name == 'id' and value == 'radiusseznam1':
                    self.printTable = True
        if tag == 'tr':
            self.column = 0
        if tag == 'a':
            self.column += 1
            if self.printTable == True:
                self.printData = True
    def handle_endtag(self, tag):
        if tag == 'span':
            self.printTable = False
        if tag == 'a':
            self.printData = False

    def handle_data(self, data):
        if self.printData == True:
            for i in range(20):
                if self.column == i:
                    self.okolica.append(data)



class MyHTMLParser_poti(HTMLParser):
    poti = []
    printData = False
    printTable = False
    def handle_starttag(self, tag, attrs):
        if tag == 'table':
            if len(attrs)==2:
                if (attrs[0][0]=='cellspacing' and attrs[0][1]=='0' and attrs[1][0]=='width' and attrs[1][1]=='100%'):
                    self.printTable = True
        if tag == 'tr':
            self.column = 0
        if tag == 'a':
            self.column += 1
            if self.printTable == True:
                self.printData = True
    def handle_endtag(self, tag):
        if tag == 'table':
            self.printTable = False
        if tag == 'a':
            self.printData = False

    def handle_data(self, data):
        if self.printData == True:
            if self.column == 1:
                self.poti.append(data)
                
            if self.column == 2:
                self.poti.append(data)
            if self.column == 3:
                self.poti.append(data)
