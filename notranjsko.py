from urllib.request import urlopen
from html.parser import HTMLParser


from poberi_podatke import MyHTMLParser
from poberi_podatke import MyHTMLParser_details
from poberi_podatke import MyHTMLParser_okolica
from poberi_podatke import MyHTMLParser_poti

from odstrani import odstrani_koncnico
from odstrani import odstrani_podatek


urlGorisko_notranjsko_sneznisko = urlopen('http://www.hribi.net/gorovje/gorisko_notranjsko_in_sneznisko_hribovje/26')
htmlGorisko_notranjsko_sneznisko = urlGorisko_notranjsko_sneznisko.read().decode('UTF-8')
parser = MyHTMLParser()
parser.feed(str(htmlGorisko_notranjsko_sneznisko))

notranjsko = parser.visine
hribi_url = parser.h_url

           
#podrobnosti o priljubljenosti posamezenga hriba, trenutni temperaturi,
#številu različnih poti in navedba hibov v radiju 2km okoli danega hriba
##details = []
##for url in hribi_url:
##    urlHrib = urlopen('http://www.hribi.net' + url)
##    htmlHrib = urlHrib.read().decode('UTF-8')
##    parser = MyHTMLParser_details()
##    parser.feed(str(htmlHrib))
##    if parser.details[26] == '\r\n\r\n':
##        details.append([odstrani_koncnico(parser.details[11]),parser.details[25],parser.details[29]])
##    else:
##        details.append([odstrani_koncnico(parser.details[11]),parser.details[26],parser.details[30]])
##    parser.details[:]=[]

    
                    
#hribi v okolici iskanega v radiju 2km
okolica = []
for url in hribi_url:
    urlHrib = urlopen('http://www.hribi.net' + url)
    htmlHrib = urlHrib.read().decode('UTF-8')
    parser = MyHTMLParser_okolica()
    parser.feed(str(htmlHrib))
    okolica.append(parser.okolica[:])
    parser.okolica[:]=[]

for seznam in okolica:
    for i in range(len(seznam)):
        seznam[i] = odstrani_podatek(seznam[i])



del notranjsko['Ime']
for ime, v in notranjsko.items():
    notranjsko[ime] = odstrani_koncnico(v)
notranjsko_seznam = sorted(notranjsko.items(), key=lambda x: (-x[1],x[0]))


#za v datoteko okolice.txt
t = list(zip(notranjsko_seznam, okolica))
for podatki in t:
    print(podatki[0][0] + ';' + ','.join(podatki[1]))


##for x in list(zip(notranjsko_seznam, details, okolica)):
##    p = []
##    for z in x[0]:
##        p.append(z)
##    for y in x[1]:
##        p.append(y)
##    p.append(x[2])
##    print(p[0],p[1],p[2],p[3],p[4],p[5])
##            
##
##    
##poti = []
##for url in hribi_url:
##    urlHrib = urlopen('http://www.hribi.net' + url)
##    htmlHrib = urlHrib.read().decode('UTF-8')
##    parser = MyHTMLParser_poti()
##    parser.feed(str(htmlHrib.replace("&nbsp;", " ")))
##    poti.append(parser.poti[:])
##    parser.poti[:]=[]
##
## 
##
##    
##hribi_poti = []
##s = list(zip(notranjsko_seznam, poti))
##for podatki in s:
##    i=0
##    while i+3<=len(podatki[1]):
##        hribi_poti.append([podatki[0][0]] + podatki[1][i:i+3])
##        i+=3
